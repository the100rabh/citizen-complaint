package com.bangalore.civic.complaint.box;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.actionbarsherlock.app.SherlockActivity;

public class GetLocationActivity extends SherlockActivity {

	protected static final int PROGRESS_DIALOG = 111;

	protected static final int FINALISE_LOCATION = 0;

	protected static final int GET_LOCATION_FAILED = 112;

	private MyLocationListener mlocListener;
	private LocationManager mlocManager;
	private ArrayList<Location> locationList = new ArrayList<Location>();

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			if (msg.what == FINALISE_LOCATION) {
				if (locationList.size() > 1) {
					Location bestLocation = locationList.get(0);
					locationList.remove(0);
					for (Location location : locationList) {
						if (location.getAccuracy() > bestLocation.getAccuracy()) {
							bestLocation = location;
						}
					}
					Intent newIntent = new Intent(GetLocationActivity.this,
							PhotoCaptureActivity.class);
					newIntent.putExtras(GetLocationActivity.this.getIntent());
					newIntent.putExtra(IntentExtraConstants.LATITUDE,
							bestLocation.getLatitude());
					newIntent.putExtra(IntentExtraConstants.LONGITUDE,
							bestLocation.getLongitude());
					GetLocationActivity.this.startActivity(newIntent);
					dismissDialog(PROGRESS_DIALOG);
				} else if (locationList.size() == 1) {
					Location bestLocation = locationList.get(0);
					Intent newIntent = new Intent(GetLocationActivity.this,
							PhotoCaptureActivity.class);
					newIntent.putExtras(GetLocationActivity.this.getIntent());
					newIntent.putExtra(IntentExtraConstants.LATITUDE,
							bestLocation.getLatitude());
					newIntent.putExtra(IntentExtraConstants.LONGITUDE,
							bestLocation.getLongitude());
					GetLocationActivity.this.startActivity(newIntent);
					dismissDialog(PROGRESS_DIALOG);
				} else {
					dismissDialog(PROGRESS_DIALOG);
					showDialog(GET_LOCATION_FAILED);
					removeAllGPSListeners();
				}
			}
		}
	};

	protected MyLocationListener mNetworkLocListener;

	protected MyLocationListener mGPSLocListener;

	protected MyLocationListener mPassiveLocListener;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_get_location);
		((Button) findViewById(R.id.button1))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Criteria criteria = new Criteria();
						criteria.setAccuracy(Criteria.ACCURACY_COARSE);
						mGPSLocListener = new MyLocationListener(
								GetLocationActivity.this);
						mNetworkLocListener = new MyLocationListener(
								GetLocationActivity.this);
						mPassiveLocListener = new MyLocationListener(
								GetLocationActivity.this);
						mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
						String bestProvider = mlocManager.getBestProvider(
								criteria, true);

						mlocManager.requestLocationUpdates(
								LocationManager.GPS_PROVIDER, 10, 0,
								mGPSLocListener);
						mlocManager.requestLocationUpdates(
								LocationManager.NETWORK_PROVIDER, 10, 0,
								mNetworkLocListener);
						mlocManager.requestLocationUpdates(
								LocationManager.PASSIVE_PROVIDER, 10, 0,
								mPassiveLocListener);
						showDialog(PROGRESS_DIALOG);
						handler.sendEmptyMessageDelayed(FINALISE_LOCATION,
								60 * 1000);
						Location loc = mlocManager
								.getLastKnownLocation(LocationManager.GPS_PROVIDER);
						if (loc != null) {
							locationList.add(loc);
						}
						loc = mlocManager
								.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
						if (loc != null) {
							locationList.add(loc);
						}
						loc = mlocManager
								.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
						if (loc != null) {
							locationList.add(loc);
						}
					}
				});
		((Button) findViewById(R.id.button2))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent newIntent = new Intent(GetLocationActivity.this,
								PhotoCaptureActivity.class);
						newIntent.putExtras(GetLocationActivity.this
								.getIntent());
						GetLocationActivity.this.startActivity(newIntent);
					}
				});

	}

	protected void removeAllGPSListeners() {
		mlocManager.removeUpdates(mGPSLocListener);
		mlocManager.removeUpdates(mNetworkLocListener);
		mlocManager.removeUpdates(mPassiveLocListener);
	}

	@Override
	@Deprecated
	protected Dialog onCreateDialog(int id) {
		if (id == PROGRESS_DIALOG) {

			return ProgressDialog.show(this, "Getting Location", "", true,
					true, new OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							finish();
						}
					});
		} else if (id == GET_LOCATION_FAILED) {
			return new AlertDialog.Builder(this)
					.setTitle("Error")
					.setMessage(
							"Failed to get location from system. Please enable all location providers and try again.")
					.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dismissDialog(GET_LOCATION_FAILED);
								}
							}).create();
		}
		return super.onCreateDialog(id);
	}

	public class MyLocationListener implements LocationListener {

		private GetLocationActivity getLocationActivity;

		public MyLocationListener(GetLocationActivity getLocationActivity) {
			this.getLocationActivity = getLocationActivity;
		}

		@Override
		public void onLocationChanged(Location loc) {
			if (getLocationActivity.isFinishing())
				return;

			mlocManager.removeUpdates(this);
			if (loc != null) {
				getLocationActivity.locationList.add(loc);
			}
		}

		@Override
		public void onProviderDisabled(String arg0) {

		}

		@Override
		public void onProviderEnabled(String arg0) {

		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {

		}

	}
}
